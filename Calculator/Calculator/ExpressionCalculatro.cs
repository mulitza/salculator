﻿using System;


namespace Calculator
{
    class ExpressionCalculatro
    {

        public double Calculate(string line)
        {
            double a=0;
            double b=0;
            double total;
            string lineBuff="";
            bool isParentheses = false;
            int parenthesesCount = 0;

            for (int i = 0; i < line.Length; i++)
            {
                if (i == 0)
                {
                    lineBuff = "";
                    parenthesesCount = 0;
                }
                if (line[i].Equals(')'))
                {
                   throw new ArgumentException();    
                }
                if (line[i].Equals('('))
                {
                    parenthesesCount++;
                    if (line[i + 1].Equals(')'))
                    {
                        throw new ArgumentException();
                    }
                    isParentheses = true;
                    int j = i + 1;

                    while (!line[j].Equals(')') || parenthesesCount!=0)
                    {
                        if (line[j].Equals('('))
                        {
                            parenthesesCount++;
                        }
                            lineBuff += line[j];
                        j++;
                        if (j > line.Length-1)
                        {
                            throw new ArgumentException();
                        }
                        if (line[j].Equals(')'))
                        {
                            parenthesesCount--;
                        }
                    }

                    if (lineBuff.Length<2)
                    {
                        line = line.Remove(i, j - i + 1);
                        line = line.Insert(i, lineBuff);
                        i = -1;
                    }
                    else
                    {
                        lineBuff = Calculate(lineBuff).ToString();
                        line = line.Remove(i, j - i + 1);
                        line = line.Insert(i, lineBuff);
                        i = -1;
                    }    
                }
                else
                {
                    isParentheses = false;
                }

                
            }
            if (isParentheses == false)
            {
                lineBuff = line;
            }



            for (int i = 0; i < lineBuff.Length; i++)
            {
                if (lineBuff[i].Equals('*') || lineBuff[i].Equals('/'))
                {
                    if(lineBuff[i+1].Equals('*') || lineBuff[i+1].Equals('/')|| lineBuff[i+1].Equals('+') || lineBuff[i + 1].Equals('-'))
                    {
                        throw new ArgumentException();
                    }

                    int j = i + 1;
                    int point = 0;
                    while (j < lineBuff.Length && !lineBuff[j].Equals('*') && !lineBuff[j].Equals('/') && !lineBuff[j].Equals('+') && !lineBuff[j].Equals('-'))
                    {
                        if (lineBuff[j].Equals('.') || lineBuff[j].Equals(','))
                        {
                            point = 1;
                            j++;
                            continue;
                        }
                        if (point > 0)
                        {
                            b += char.GetNumericValue(lineBuff[j]) / Math.Pow(10, j - i - point);
                            point++;
                        }
                        else
                        {
                            b += char.GetNumericValue(lineBuff[j]) / Math.Pow(10, j - i);
                        }
                        j++;
                    }
                    j--;

                    b = b * Math.Pow(10, j - i - point);


                    int k = i - 1;
                    point = 0;
                    while (k > -1 && !lineBuff[k].Equals('+') && !lineBuff[k].Equals('-') && !lineBuff[k].Equals('*') && !lineBuff[k].Equals('/'))
                    {
                        if (lineBuff[k].Equals('.') || lineBuff[k].Equals(','))
                        {
                            point = 2;
                            a = a / Math.Pow(10, k - i + 3);
                            k--;
                            continue;
                        }
                        a += char.GetNumericValue(lineBuff[k]) * Math.Pow(10, i - k - 1 - point);
                        k--;
                    }
                    k++;


                    for (int q = i - 1; q >= 0; q--)
                    {
                        if (lineBuff[q].Equals('-') && q == 0)
                        {
                            a = -a;
                        }
                        if (lineBuff[q].Equals('-') || lineBuff[q].Equals('+') || lineBuff[q].Equals('/') || lineBuff[q].Equals('*'))
                        {
                            break;
                        }
                    }

                    if (lineBuff[i].Equals('*'))
                    {
                        total = a * b;
                    }
                    else
                    {
                        total = a / b;
                    }

                    lineBuff = lineBuff.Remove(k, j - k + 1);
                    lineBuff = lineBuff.Insert(k, total.ToString());
                    i = 0;
                    b = 0;
                    a = 0;
                    
                }
            }


            if (lineBuff.Length > 0)
            {
                if ((lineBuff[0].Equals('+') || lineBuff[0].Equals('-')) && (lineBuff[1].Equals('*') || lineBuff[1].Equals('/') || lineBuff[1].Equals('+') || lineBuff[1].Equals('-')))
                {
                    throw new ArgumentException();
                }
            }

            for (int i = 0; i < lineBuff.Length; i++)
            {
                if ((lineBuff[i].Equals('+') || lineBuff[i].Equals('-')) && i != 0)
                {
                    int j = i + 1;
                    int point = 0;

                    while (j < lineBuff.Length && !lineBuff[j].Equals('*') && !lineBuff[j].Equals('/') && !lineBuff[j].Equals('+') && !lineBuff[j].Equals('-'))
                    {
                        if (lineBuff[j].Equals('.') || lineBuff[j].Equals(','))
                        {
                            point = 1;
                            j++;
                            continue;
                        }
                        if (point > 0)
                        {
                            b += char.GetNumericValue(lineBuff[j]) / Math.Pow(10, j - i - point);
                            point++;
                        }
                        else
                        {
                            b += char.GetNumericValue(lineBuff[j]) / Math.Pow(10, j - i);
                        }

                        j++;
                    }
                    j--;

                    b = b * Math.Pow(10, j - i - point);


                    int k = i - 1;
                    point = 0;

                    while (k > -1 && !lineBuff[k].Equals('*') && !lineBuff[k].Equals('/') && !lineBuff[k].Equals('+') && !lineBuff[k].Equals('-'))
                    {
                        if (lineBuff[k].Equals('.') || lineBuff[k].Equals(','))
                        {
                            point = 2;
                            a = a / Math.Pow(10, k - i + 3);
                            k--;
                            continue;
                        }
                        a += char.GetNumericValue(lineBuff[k]) * Math.Pow(10, i - k - 1 - point);
                        k--;
                    }
                    k++;

                    for (int q = i - 1; q >= 0; q--)
                    {
                        if (lineBuff[q].Equals('-') && q == 0)
                        {
                            a = -a;
                            k--;
                        }
                        if (lineBuff[q].Equals('-') || lineBuff[q].Equals('+') || lineBuff[q].Equals('/') || lineBuff[q].Equals('*'))
                        {
                            break;
                        }
                    }

                    if (lineBuff[i].Equals('-'))
                    {
                        total = a - b;
                    }
                    else
                    {
                        total = a + b;
                    }


                    lineBuff = lineBuff.Remove(k, j - k + 1);
                    lineBuff = lineBuff.Insert(k, total.ToString());
                    i = 0;
                    b = 0;
                    a = 0;
                }
            }

            return double.Parse(lineBuff);
        }
    }
}
