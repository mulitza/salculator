﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    internal class Program
    {
        static void Main()
        {
            var calculator = new ExpressionCalculatro();
            
            Console.WriteLine("Введите выражение:");
            var input = Console.ReadLine();
            
            try
            {
                var result = calculator.Calculate(input);
                Console.WriteLine(result);
            }
            catch(ArgumentException)
            {
                Console.WriteLine("Введено некорректное выражение");
            }    
            Console.ReadKey();
        }


       
    }
}
